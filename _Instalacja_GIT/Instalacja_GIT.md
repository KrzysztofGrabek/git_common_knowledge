# GIT Instalacja

Pobierz ze strony (http://www.git-scm.com) plik instalacyjny GIT-a dla twojego systemu operacyjnego.

!!! BARDZO WAŻNE 
    1. Przy instalacji zaznacz -> Windows Explorer Integration -> Git Bash Here
		-> Ta opcja doda konsolę GIT pod prawy przycisk myszy. 
    2. Use GIT From The windows Command Prompt 
    3. Terminal Emulator: MinTTY
    4. Nie zaleca się instalowania opcji ekperymentalnych

        Ps. Wszysktie ustawienia mogą być zienione później

Po instalcaji uruchom konsolę Windowsa CMD i wpisz komendę 
 -> git version 
 
 
 
 
 ![image](Images/GIT_VERSION.PNG)
    
